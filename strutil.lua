function lines(s)
    if s:sub(-1) ~= "\n" then s = s .. "\n" end
    return s:gmatch("(.-)\n")
end

function find_last(s, t, plain)
    local a, b = nil, nil
    local i, j = s:find(t, 1, plain)
    while i do
        a, b = i, j
        i, j = s:find(t, j+1, plain)
    end
    return a, b
end

function find_last_any(s, ts, plain)
    local a, b = nil, nil
    for ti = 1, #ts do
        local i, j = find_last(s, ts[ti], plain)
        if j and (not b or j > b) then a, b = i, j end
    end
    return a, b
end
