Turns otherwise valid Lua code with +=, -= and other compound assignment operators into regular Lua code.

Known issues:

- block comments
- escaped quotes
- missing spaces around "do", "repeat" or "then"