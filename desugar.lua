require "strutil"
--local print = function(...) end

local function get_quote_ranges(s) -- TODO: ignore escaped quotes.

    local rs, qs = {}, {"'", "\""}
    local find_start, rn, qn = 1, 0, #qs

    while true do

        local mni, mnc = nil, nil
        for qi = 1, qn do
            local i = s:find(qs[qi], find_start, true)
            if i and (not mni or i < mni) then
                mni = i
                mnc = qs[qi]
            end
        end
        if not mni then return rs end

        local j = s:find(mnc, mni+1, true)
        rs[rn+1] = mni
        rn = rn + 2
        if not j then
            print("Warning: unmatched quote " .. mnc .. " at index " ..
                  tostring(mni) .. " in string " .. s)
            rs[rn] = #s
            return rs
        end
        rs[rn] = j
        find_start = j+1
    end
end

local function find_not_in_ranges(s, c, rs, find_start)

    find_start = find_start or 1
    local range_start, rn = 1, #rs

    while true do
        local i, j = s:find(c, find_start, true)
        if not i then return nil end

        local ri = range_start
        while ri < rn and rs[ri+1] <= i do ri = ri + 2 end
        if ri >= rn or rs[ri] >= i then return i, j end
        -- Use > instead of >= ?

        assert(rs[ri] < j and j < rs[ri + 1])
        range_start = ri
        find_start = j + 1
    end
end

-- TODO: --[[]] comments
local function find_code(s, c, rs, find_start)

    rs = rs or get_quote_ranges(s)

    local i, j = find_not_in_ranges(s, c, rs, find_start)
    if not i then return nil end

    local comment_start = find_not_in_ranges(s, "--", rs, find_start)
    if comment_start and i > comment_start then return nil end

    return i, j
end

-- TODO: don't rely on spaces around "then", "do" and "repeat".
local function desugar_str(s, op, rs)

    local w = s:find("%S")
    local i, j = find_code(s, op .. "=", rs, w)
    if not i then return nil end
    -- At this point w can't be nil, since then the whole
    -- string would consist of whitespace.

    local _, b = find_last_any(s, {"%sthen%s", "%sdo%s", "%srepeat%s", ";"})
    if not b then
        b = w - 1
    else
        assert(b >= w, "Keyword found before whitespace (?)")
    end

    local pre = ((b > 0) and s:sub(1, b)) or ""
    local lhs, post = s:sub(b+1, i-1), s:sub(j+1)
    if true then
        print(lhs)
        print(op)
        print(post .. "\n")
    end

    return pre .. lhs .. "= " .. lhs .. op .. post
end

local function desugar_file(ops, src, dst)

    local sf = assert(io.open(src, "r"))
    local df = assert(io.open(dst, "w"))
    local ss = sf:read("*all")

    local opn = #ops
    for l in lines(ss) do
        local s = nil
        local rs = get_quote_ranges(l)
        for i = 1, opn do
            s = desugar_str(l, ops[i], rs)
            if s then break end
        end
        df:write((s or l) .. "\n")
    end

    sf:close()
    df:close()
end

local ops = {"+", "-", "*", "/", "%", "..", "or"}
desugar_file(ops, "sugar.lua", "desugared.lua")
